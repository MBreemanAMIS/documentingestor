export interface IDocument {
    id: string;
    status: string;
    type: string;
    indicationDamaged: boolean;
    value: number;
    writeOffValue: number;
    creationDate: string;
    owner: ICompany;
    damage?: {
        typeOfDamage: IDamageType;
        description: string;
        indicationRestored: boolean;
        dateOfDamage: Date;
        dateOfRestoration?: Date;
        placeOfDamage: string;
        estDamageValue?: number;
        realDamageValue?: number;
        reparedBy?: ICompany;
    }

}

interface IDamageType {
    type: string;
    description: string;
}

interface ICompany {
    id: string;
    name: string;
    streetName: string;
    city: string;
    country: string;
    postalCode: string;
    phoneNumber: string;
    kvkNumber: string
}