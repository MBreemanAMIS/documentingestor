import { MongoClient, ObjectId } from 'mongodb';
import { insertInMongo } from './index';
import { IDocument } from './document'
import initConfig from './config';
import { expect } from 'chai'

describe('Mongo', () => {
  const config = initConfig()
  const docId = new ObjectId('dit is test1')

  it('inserts a doc into mongo', async () => {
    const doc: IDocument = {
      id: '1',
      status: 'in transit',
      type: 'krat',
      indicationDamaged: false,
      value: 100.39,
      writeOffValue: 24.56,
      creationDate: '2001-01-01',
      owner: {
        id: '654',
        name: 'Test inc.2',
        streetName: 'test st.',
        city: 'Cairo',
        country: 'Egypt',
        postalCode: '90210',
        phoneNumber: '0611111111',
        kvkNumber: '1'
      }
    }
    const responseMessage = await insertInMongo(console.log, doc, docId)
    expect(responseMessage).to.equal('You have uploaded a container with id: 1')
    const client = await MongoClient.connect(config.mongoUrl)
    const res = await client.db('containeris').collection('identification').findOne({ _id: docId })
    expect(res._id.toHexString()).to.eq(docId.toHexString())
    client.close()
  });

  it('Overwrites an existing document if _id is passed in', async () => {
    const doc: IDocument = {
      id: '1',
      status: 'sent',
      type: 'krat',
      indicationDamaged: false,
      value: 100.39,
      writeOffValue: 24.56,
      creationDate: '2001-01-01',
      owner: {
        id: '654',
        name: 'Test inc.2',
        streetName: 'test st.',
        city: 'Cairo',
        country: 'Egypt',
        postalCode: '90210',
        phoneNumber: '0611111111',
        kvkNumber: '1'
      }
    }
    const client = await MongoClient.connect(config.mongoUrl)
    const responseMessage = await insertInMongo(console.log, doc, docId)
    expect(responseMessage).to.equal('You have uploaded a container with id: 1')
    const res = await client.db('containeris').collection('identification').findOne({ _id: docId })
    expect(res.status).to.eq('sent')
    client.close()

  })
});
