import { AzureFunction, Context, HttpRequest } from '@azure/functions';
import { MongoClient, ObjectId } from 'mongodb';
import initConfig from './config';
import { IDocument } from './document';

// eslint-disable-next-line func-names
const httpTrigger: AzureFunction = async function (
  context: Context,
  req: HttpRequest,
): Promise<void> {
  const log = context.log;
  const document = req.body
  let responseMessage
  if (document?._id) {
    const id = document._id;
    delete document._id
    responseMessage = await insertInMongo(log, document, id);
  } else (responseMessage = await insertInMongo(log, document))


  context.res = {
    // status: 200, /* Defaults to 200 */
    body: responseMessage,
  };
};

function mandatoryFieldsPresent(doc: IDocument){
  if (doc.hasOwnProperty('status') && doc.hasOwnProperty('type') && doc.hasOwnProperty('identificatie')) {
    return true;
  }
  else {
    return false;
  };
};

export async function insertInMongo(log, document: IDocument, docId = new ObjectId): Promise<string> {
  let responseMessage = '';
  if (mandatoryFieldsPresent(document)){
    log('HTTP trigger function processed a request.');
    const config = initConfig();
    const { mongoUrl } = config;
    log('mongoUrl :>> ', mongoUrl);

    const client = await MongoClient.connect(mongoUrl)
    // const res = await client.db('containeris').collection('identification').insertOne(document);
    const res = await client.db('containeris').collection('identification').updateOne({ _id: docId }, { $set: document }, { upsert: true });

    await client.close();
    if (res.result.n === res.result.ok) {
      log('Inserted your document');
      responseMessage = `You have uploaded a container with id: ${document.id}`
    } else {
      log('failed inserting all documents')
      responseMessage = `Failed uploading container with id: ${document.id}`
    }
  }
  else {
    responseMessage = "You have tried to upload a container missing data.";
  }
  return responseMessage;
}

export default httpTrigger;
