export interface IConfig {
    mongoUrl: string;
}

function initConfig(): IConfig {
    const config: IConfig = {
        mongoUrl: undefined,
    };
    if (process.env.NODE_ENV === 'test') {
        config.mongoUrl = 'mongodb://containerCase:container@127.0.0.1:27017/containeris';
    } else {
        config.mongoUrl = 'mongodb://containeris:hFof3N8b1SDxfcyQpOZcQCk8XSoXPDWL0I8KoKxWPZpY7Z25i2zkRdCIu1WCIwnCv8NN4Cyghd3kLbrLTebNhw==@containeris.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@containeris@';
    }
    return config;
}

export default initConfig;
